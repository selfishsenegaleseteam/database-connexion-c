﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ListeCommandes
{
    class CommandesClient
    {
        static void Main(string[] args)
        {
            SqlConnection dataConn = new SqlConnection();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = ".\\SQLExpress";
                builder.InitialCatalog = "Northwind";
                builder.IntegratedSecurity = true;
                dataConn.ConnectionString = builder.ConnectionString;
                dataConn.Open();
                Console.Write("Entrez un code client SVP 5 Car. : ");
                String customerID = Console.ReadLine();
                SqlCommand dataCommand = new SqlCommand();
                dataCommand.Connection = dataConn;
                dataCommand.CommandText =
                "Select OrderID,OrderDate,ShippedDate,Shipname,ShipAddress,ShipCity,ShipCountry " +
                "From Orders Where CustomerID = @CustomerIdParam";
                SqlParameter param = new SqlParameter("@CustomerIdParam", SqlDbType.Char, 5);
                param.Value = customerID;
                dataCommand.Parameters.Add(param);
                Console.WriteLine("Prêt à exécuter la requête : {0}\n\n", dataCommand.CommandText);
                SqlDataReader dataReader = dataCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    int orderID = dataReader.GetInt32(0);
                    DateTime dateCommande = dataReader.GetDateTime(1);
                    if (dataReader.IsDBNull(2))
                    {
                        Console.WriteLine("Commande {0} non encore livree", orderID);
                        String nomLivraison = dataReader.GetString(3);
                        String adrLivraison = dataReader.GetString(4);
                        String villeLivraison = dataReader.GetString(5);
                        String paysLivraison = dataReader.GetString(6);
                        Console.WriteLine("Commande: {0}\nPlacee le: {1}\nLivree le: \n l’Adresse: {2}\n{3}\n{4}\n{5}\n\n", orderID, dateCommande, nomLivraison, adrLivraison, villeLivraison, paysLivraison);
                    }
                    else
                    {
                        DateTime dateLivraison = dataReader.GetDateTime(2);
                        String nomLivraison = dataReader.GetString(3);
                        String adrLivraison = dataReader.GetString(4);
                        String villeLivraison = dataReader.GetString(5);
                        String paysLivraison = dataReader.GetString(6);
                        Console.WriteLine("Commande: {0}\nPlacee le: {1}\nLivree le: {2}\n l’Adresse: {3}\n{4}\n{5}\n{6}\n\n", orderID, dateCommande, dateLivraison, nomLivraison, adrLivraison, villeLivraison, paysLivraison);
                    }
                }
                dataReader.Close();
            }
            catch (SqlException e)
            {
                Console.WriteLine("Erreur accès à la Base Northwind : {0} ", e.Message);
            }
            finally {
                dataConn.Close();
            }
        }
    }
}
