﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace ListeCommandesMysql
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string connectionString = "SERVER=127.0.0.1; DATABASE=northwind; UID=root; PASSWORD='';";
                MySqlConnection connection = new MySqlConnection(connectionString);
                connection.Open();
                Console.Write("Entrez un code client SVP 5 Car. : ");
                string customerID = Console.ReadLine();
                MySqlCommand dataCommand = connection.CreateCommand();
                dataCommand.CommandText = "Select OrderID,OrderDate,ShippedDate,Shipname,ShipAddress,ShipCity,ShipCountry " +
                "From Orders Where CustomerID = @CustomerIdParam";
                dataCommand.Parameters.AddWithValue("@CustomerIdParam", customerID);
                Console.WriteLine("Prêt à exécuter la requête : {0}\n\n", dataCommand.CommandText);
                MySqlDataReader dataReader = dataCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    int orderID = dataReader.GetInt32(0);
                    DateTime dateCommande = dataReader.GetDateTime(1);
                    if (dataReader.IsDBNull(2)) {
                        Console.WriteLine("Commande {0} non encore livree", orderID);
                        String nomLivraison = dataReader.GetString(3);
                        String adrLivraison = dataReader.GetString(4);
                        String villeLivraison = dataReader.GetString(5);
                        String paysLivraison = dataReader.GetString(6);
                        Console.WriteLine("Commande: {0}\nPlacee le: {1}\nLivree le: \n l’Adresse: {2}\n{3}\n{4}\n{5}\n\n", orderID, dateCommande, nomLivraison, adrLivraison, villeLivraison, paysLivraison);
                    }
                    else
                    {
                        DateTime dateLivraison = dataReader.GetDateTime(2);
                        String nomLivraison = dataReader.GetString(3);
                        String adrLivraison = dataReader.GetString(4);
                        String villeLivraison = dataReader.GetString(5);
                        String paysLivraison = dataReader.GetString(6);
                        Console.WriteLine("Commande: {0}\nPlacee le: {1}\nLivree le: {2}\n l’Adresse: {3}\n{4}\n{5}\n{6}\n\n", orderID, dateCommande, dateLivraison, nomLivraison, adrLivraison, villeLivraison, paysLivraison);
                    }
                    
                }
                dataReader.Close();
                connection.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine("Erreur accès à la Base Northwind : {0} ", e.Message);
            }
            finally
            {
                
                Console.ReadKey();
            }
        }
    }
}
